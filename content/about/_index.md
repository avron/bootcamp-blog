---
title: "About"
date: 2019-10-29T13:49:23+06:00
draft: false

# image
image: "images/logo.png"

# meta description
description: "this is meta description"

# type
type : "about"
---
Startup Bootcamp SJCET aims to promote the entrepreneurship and innovative creature culture among students of St. Joseph's College of Engineering & Technology, Palai . Here, students engage themselves in conducting events, doing projects apart from academics and develop a maker culture among the students. This is a sub body under the Kerala startup Mission powered by Kerala Government.
